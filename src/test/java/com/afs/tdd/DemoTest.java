package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class DemoTest {
    @Test
    void should_change_to_0_1_north_when_executeCommand_given_location_0_0_N_and_command_move() {
        //given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;

        Location locationAfterMove = marsRover.executeCommand(commandMove);

        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_west_when_executeCommand_given_location_0_0_N_and_command_left() {
        //given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Left;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_east_when_executeCommand_given_location_0_0_N_and_command_right() {
        //given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Right;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());

    }

    @Test
    void should_change_to_0_minus1_south_when_executeCommand_given_location_0_0_S_and_command_move() {
        //given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(-1, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());

    }

    @Test
    void should_change_to_0_0_e_when_executeCommand_given_location_0_0_S_and_command_left() {
        //given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Left;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());

    }

    @Test
    void should_change_to_0_0_w_when_executeCommand_given_location_0_0_S_and_command_right() {
        //given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Right;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());

    }

    @Test
    void should_change_to_0_0_n_when_executeCommand_given_location_0_0_e_and_command_left() {
        //given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Left;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());

    }

    @Test
    void should_change_to_0_0_s_when_executeCommand_given_location_0_0_e_and_command_right() {
        //given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Right;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_minus1_0_w_when_executeCommand_given_location_0_0_w_and_command_move() {
        //given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(-1, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_s_when_executeCommand_given_location_0_0_w_and_command_left() {
        //given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Left;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_n_when_executeCommand_given_location_0_0_w_and_command_right() {
        //given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Right;
        //when
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_minus1_1_n_when_executeBatchCommands_given_0_0_n_and_command_MLMR(){
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        List<Command> commands = new ArrayList<>();
        commands.add(Command.Move);
        commands.add(Command.Left);
        commands.add(Command.Move);
        commands.add(Command.Right);

        Location locationAfterMove = marsRover.executeBatchCommands(commands);

        Assertions.assertEquals(-1,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }
}
