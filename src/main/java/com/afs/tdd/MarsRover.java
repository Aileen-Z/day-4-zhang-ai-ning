package com.afs.tdd;

import java.util.List;

public class MarsRover {
    Location location;
    MarsRover(Location location){
        this.location = location;
    }

    public Location executeCommand(Command commandMove) {
        if(commandMove==Command.Move){
            if(location.getDirection() == Direction.North) {
                location.setCoordinateY(location.getCoordinateY() + 1);
            }else if(location.getDirection() == Direction.South){
                location.setCoordinateY(location.getCoordinateY() - 1);
            }else if(location.getDirection() == Direction.East){
                location.setCoordinateX(location.getCoordinateX() + 1);
            }else if(location.getDirection() == Direction.West){
                location.setCoordinateX(location.getCoordinateX() - 1);
            }

        }

        if(commandMove==Command.Left){
            if(location.getDirection() == Direction.North){
                location.setDirection(Direction.West);
            }else if(location.getDirection() == Direction.South){
                location.setDirection(Direction.East);
            }else if(location.getDirection() == Direction.East){
                location.setDirection(Direction.North);
            }else if(location.getDirection() == Direction.West){
                location.setDirection(Direction.South);
            }
        }

        if(commandMove == Command.Right){
            if(location.getDirection() == Direction.North){
                location.setDirection(Direction.East);
            }else if(location.getDirection() == Direction.South){
                location.setDirection(Direction.West);
            }else if(location.getDirection() == Direction.East){
                location.setDirection(Direction.South);
            }else if(location.getDirection() == Direction.West){
                location.setDirection(Direction.North);
            }
        }
        return location;
    }

    public Location executeBatchCommands(List<Command> commands){
        for(Command c : commands){
            Location tmpLocation = executeCommand(c);
        }
        return location;
    }
}
